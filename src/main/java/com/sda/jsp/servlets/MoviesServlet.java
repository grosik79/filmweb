package com.sda.jsp.servlets;

import com.sda.jsp.com.sda.jsp.model.Movies;
import com.sda.jsp.dao.DatabaseSession;
import com.sda.jsp.dao.MoviesDao;
import com.sun.deploy.net.HttpRequest;
import com.sun.deploy.net.HttpResponse;
import org.hibernate.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/movies")
public class MoviesServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        MoviesDao dao = new MoviesDao();

        try {
            List<Movies> movies = dao.getAllMovies();
//
//            PrintWriter writer = response.getWriter();
//
//            writer.println("<h1> Hello World!</h1>");
//            writer.flush();
//
            request.setAttribute("movies", movies);
            request.getRequestDispatcher("/movies.jsp").forward(request, response);
        }catch (IOException e){
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
         String name = req.getParameter("name");
        String director = req.getParameter("director");
        Double rate = Double.valueOf(req.getParameter("rate"));
        Session session = DatabaseSession.getSessionFactory().openSession();
        session.saveOrUpdate(new Movies(name, rate));
        session.close();
        resp.sendRedirect("/movies");
    }


}