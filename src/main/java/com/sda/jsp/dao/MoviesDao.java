package com.sda.jsp.dao;

import com.sda.jsp.com.sda.jsp.model.Movies;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class MoviesDao {
    public List<Movies> getAllMovies(){
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Criteria criteria = session.createCriteria(Movies.class);
        List<Movies> moviesList = criteria.list();
                return moviesList;
    }
    public void putMockObject(){
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(new Movies());
transaction.commit();

session.close();

    }
}
