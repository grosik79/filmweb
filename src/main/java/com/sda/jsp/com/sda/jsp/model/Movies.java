package com.sda.jsp.com.sda.jsp.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Movies {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;


    @OneToOne(fetch = FetchType.EAGER)
    private Person director;

    @Column
    private Date releaseDate;

    @Column
    private double rate;

    public Movies(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setRelease(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public Movies(String name, Person director, double rate) {
        this.name = name;
        this.director = director;
        this.rate = rate;
    }

    public Movies(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }
}
